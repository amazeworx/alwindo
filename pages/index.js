import Head from 'next/head'
import Image from 'next/image'
import useViewport from '../hooks/useViewport.js'

const Home = () => {
  const { width } = useViewport();
  const breakpoint = 1024;

  if (width < breakpoint) {
    return (
      <>
        <Head>
          <title>Alwindo Prima Solusi</title>
          <meta name="description" content="Alwindo Prima Solusi. Commited to work & serve from the heart" />
        </Head>
        <main className="mobile-homepage lg:hidden">

          <div id="top">
            <div className="mobile-home-hero">

            </div>
          </div>

          <div id="about">
            <div className="flex justify-center border-b border-primary border-opacity-30">
              <div className="px-12 py-20">
                <h2 className="text-3xl text-primary font-semibold mb-6">What We Do</h2>
                <h3 className="text-2xl text-gray-500 mb-6">What we do <br />is simple:</h3>
                <h3 className="text-2xl text-gray-500 mb-6">We build <br />network</h3>
                <h3 className="text-2xl text-gray-500 mb-6">We amp <br />security</h3>
                <h3 className="text-2xl text-gray-500 mb-3">We create <br />solutions</h3>
              </div>
            </div>

            <div id="vision-mission">
              <div className="flex justify-center">
                <div className="max-w-md px-12 py-20">
                  <h2 className="text-xl text-primary font-semibold uppercase mb-2">Vision</h2>
                  <p className="text-lg">Our vision is to lead the world with the technology of the future.</p>
                  <h2 className="text-xl text-primary font-semibold uppercase mb-2 mt-20">Mission</h2>
                  <p className="text-lg">We strive to be the leading IT solution company with commitment to support our customer by providing innovative, reliable and best-in-class IT solutions to help their business go further.</p>
                </div>
              </div>
            </div>

            <div id="services" className="flex flex-col bg-primary text-white">

              <div className="px-5 pt-10">
                <h2 className="text-xl font-bold">THE JOURNEY</h2>
                <h3 className="text-sm">IN CREATING YOUR IT INFRASTRUCTURE</h3>
                <div className="border-b border-white border-opacity-30 my-4"></div>
                <p>In creating the best infrastructure to your company information technology sector, the journey is not an instant growth. It has a parallel track to match ever evolving progress the company grows.</p>
              </div>

              <div className="flex flex-col px-5 pt-10 mt-8 border-b border-white border-opacity-50">

                <div className="max-w-xs">
                  <a href="/services/end-point" className="service-link pb-12">
                    <div className="absolute -top-6 -left-3 bottom-0">
                      <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                      <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">1</div>
                    </div>
                    <div className="relative z-30 mb-3 -ml-5 pt-6" style={{ maxWidth: 200 + 'px' }}>
                      <Image src="/img-services-1.png" width={300} height={265} />
                    </div>
                    <h2 className="text-xl uppercase mb-3">
                      <span className="font-light">End Point</span> <span className="font-bold">Solutions</span>
                    </h2>
                    <p className="text-sm">Front end user computing refers to all of the customer need that handled along by user example: Personal computer, notebook/laptop, printing solutions and handheld devices.</p>
                  </a>
                </div>

                <div className="max-w-xs">
                  <a href="/services/data-center-infrastructure" className="service-link pb-12">
                    <div className="absolute -top-6 -left-3 bottom-0">
                      <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                      <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">2</div>
                    </div>
                    <div className="relative z-30 mb-3 -ml-5 pt-6" style={{ maxWidth: 240 + 'px', paddingLeft: 1 + 'px' }}>
                      <Image src="/img-services-2.png" width={300} height={242} />
                    </div>
                    <h2 className="text-xl uppercase mb-3">
                      <span className="font-light">Data Center & Infrastructure</span> <span className="font-semibold">Solutions</span>
                    </h2>
                    <p className="text-sm">Front end user computing refers to all of the customer need that handled along by user example: Personal computer, notebook/laptop, printing solutions and handheld devices.</p>
                  </a>
                </div>

                <div className="max-w-xs">
                  <a href="/services/availability-data-protection" className="service-link pb-12">
                    <div className="absolute -top-6 -left-3 bottom-0">
                      <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                      <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">3</div>
                    </div>
                    <div className="relative z-30 mb-3 -ml-5">
                      <Image src="/img-services-3c.png" width={360} height={180} />
                    </div>
                    <h2 className="text-xl uppercase mb-3">
                      <span className="font-light">Availability & Data Protection</span> <span className="font-semibold">Solutions</span>
                    </h2>
                    <p className="text-sm">Data protection is the process of safeguarding important information from corruption, compromise or loss. The importance of data protection increases as the amount of data created and stored continues to grow at unprecedented rates. There is also little tolerance for downtime that can make it impossible to access important information.</p>
                  </a>
                </div>

                <div className="max-w-xs">
                  <a href="/services/network" className="service-link pb-16">
                    <div className="absolute -top-6 -left-3 bottom-0">
                      <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                      <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">4</div>
                    </div>
                    <div className="relative z-30 mb-3 -ml-5 pt-0" style={{ maxWidth: 360 + 'px', paddingLeft: 10 + 'px' }}>
                      <Image src="/img-services-4.png" width={360} height={264} />
                    </div>
                    <h2 className="text-xl uppercase mb-3">
                      <span className="font-light">Network</span> <span className="font-semibold">Solutions</span>
                    </h2>
                    <p className="text-sm">Solutions that has critical role in business and communications. Networks provide speed, connectivity and ultimately value to their members and solutions to business challenges.</p>
                  </a>
                </div>

                <div className="max-w-xs">
                  <a href="/services/cyber-security" className="service-link pb-12">
                    <div className="absolute -top-6 -left-3 bottom-0">
                      <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                      <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">5</div>
                    </div>
                    <div className="relative z-30 mb-3 -ml-16">
                      <Image src="/img-services-5b.png" width={360} height={189} />
                    </div>
                    <h2 className="text-xl uppercase mb-3">
                      <span className="font-light">Cyber Security</span> <span className="font-semibold">Solutions</span>
                    </h2>
                    <p className="text-sm">Cyber security refers to a company’s protection against unauthorized or criminal use of electronic data and cyber security services are the over-arching processes put in place to achieve this security and protect against common cyber threats.</p>
                  </a>
                </div>

                <div className="max-w-xs">
                  <a href="/services/other" className="service-link pb-12">
                    <div className="absolute -top-6 -left-3 bottom-0">
                      <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                      <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">6</div>
                    </div>
                    <div className="relative z-30 mb-3 ml-0 pt-0" style={{ maxWidth: 220 + 'px' }}>
                      <Image src="/img-services-6.png" width={300} height={276} />
                    </div>
                    <h2 className="text-xl uppercase mb-3">
                      <span className="font-light">OTHER</span> <span className="font-semibold">Solutions</span>
                    </h2>
                    <p className="text-sm">Our other solutions.</p>
                  </a>
                </div>

              </div>

            </div>

            <div id="end-point" className="flex flex-col">

              <div className="px-5 pt-12 pb-5 bg-primary text-white">
                <div>
                  <h1 className="uppercase text-2xl mb-4">End Point<br /><span className="font-bold">Solutions</span></h1>
                </div>
                <div className="text-center">
                  <Image src="/mobile/service-1-bg.png" width={600} height={546} />
                </div>
              </div>

              <div className="px-5 py-4">
                <p className="mb-4">The equipment that being used directly by end-user to support their daily work activities.</p>
                <ul className="list-infinite">
                  <li>Desktops</li>
                  <li>Workstation</li>
                  <li>Laptops</li>
                  <li>Thin Client</li>
                  <li>Virtual Desktop</li>
                  <li>Printer</li>
                  <li>Fingerprint Scanner</li>
                </ul>
              </div>

              <div className="px-5 pt-2 pb-8">
                <div className="flex flex-wrap justify-center -mx-4">
                  <div className="flex items-center w-1/3 px-4 py-2"><Image src="/logos/acer.png" width={334} height={80} /></div>
                  <div className="flex items-center w-1/3 px-4 py-2"><Image src="/logos/lenovo.png" width={240} height={80} /></div>
                  <div className="flex items-center w-1/3 px-4 py-2"><Image src="/logos/dell.png" width={60} height={60} /></div>
                  <div className="flex items-center w-1/3 px-4 py-2"><Image src="/logos/fujitsu.png" width={161} height={80} /></div>
                  <div className="flex items-center w-1/3 px-4 py-2"><Image src="/logos/canon.png" width={385} height={80} /></div>
                </div>
              </div>

            </div>

            <div id="data-center-infrastructure" className="flex flex-col">

              <div className="px-5 pt-12 pb-5 bg-primary text-white">
                <h1 className="uppercase text-2xl mb-6">Data Center Infrastructure<br /><span className="font-bold">Solutions</span></h1>
                <div>
                  <p className="mb-4">Data Center contain many IT peripherals, computer engine is one of it, popular being called as servers. As time goes by, technologies has grown wider, trying to integrate one to another.</p>
                  <ul className="list-infinite">
                    <li>Servers</li>
                    <li>Integrated Infrastructure System</li>
                    <li>Hyper converged System</li>
                  </ul>
                  <p className="mt-8 mb-4">The z Storage is the computerized data storage system for large scale, intended as centralized storage (consolidation) system in every organization to store billions of data.</p>
                  <ul className="list-infinite">
                    <li>Storage Area Network (Block Based Storage)</li>
                    <li>Network Attached Storage (File Based Storage)</li>
                    <li>Object Storage (Object Based Storage)</li>
                    <li>Backup Storage (Storage for Backup Solution)</li>
                  </ul>
                </div>
              </div>

              <div className="text-right">
                <Image src="/mobile/service-2-bg.png" width={600} height={400} />
              </div>

              <div className="px-5 pt-5 pb-10 -mt-12">
                <div className="grid grid-cols-3 gap-x-10 gap-y-6">
                  <div className="logo-partner"><Image src="/logos/vertiv.png" width={313} height={80} /></div>
                  <div className="logo-partner"></div>
                  <div className="logo-partner"></div>
                  <div className="logo-partner"><Image src="/logos/apc.png" width={255} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/fujitsu.png" width={161} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/lenovo.png" width={240} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/dellemc.png" width={463} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/netapp.png" width={429} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/vmware.png" width={378} height={80} /></div>
                </div>
              </div>

            </div>

            <div id="availability-data-protection" className="flex flex-col">

              <div className="px-5 pt-12 pb-0 bg-primary text-white">
                <h1 className="uppercase text-2xl mb-6">Backup Continuity<br /><span className="font-bold">Solutions</span></h1>
              </div>
              <div className="text-center">
                <Image src="/mobile/service-3-bg.png" width={600} height={600} />
              </div>
              <div className="px-5 py-4">
                <p className="mb-4">Every organization expect their businesses will always up and running. But there are few unpredictable things such as natural disaster, terrorism or unplanned system downtime. There are many technologies that can be use to prevent the disruption and to allow fast recovery in business critical application.</p>
                <ul className="list-infinite">
                  <li>Replication</li>
                  <li>High Availability Cluster</li>
                  <li>Fault Tolerance</li>
                  <li>Active-Active Data Center</li>
                  <li>Backup and recovery</li>
                  <li>Archiving</li>
                </ul>
              </div>

              <div className="px-8 pt-4 pb-8">
                <div className="grid grid-cols-2 gap-x-10 gap-y-6">
                  <div className="logo-partner"><Image src="/logos/commvault.png" width={460} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/dellemc.png" width={463} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/veeam.png" width={444} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/veritas.png" width={406} height={80} /></div>
                </div>
              </div>

            </div>

            <div id="network" className="flex flex-col">

              <div className="px-5 pt-12 pb-0 bg-primary text-white">
                <h1 className="uppercase text-2xl mb-6">Network<br /><span className="font-bold">Solutions</span></h1>
              </div>
              <div className="text-center"><Image src="/mobile/service-4-bg.png" width={600} height={400} /></div>
              <div className="-mt-2 px-5 pt-2 pb-5 bg-primary text-white">
                <ul className="list-infinite">
                  <li>WAN Optimization</li>
                  <li>Wireless Network</li>
                  <li>Network Operation Control</li>
                  <li>Application Delivery Control / Load Balance</li>
                  <li>Routing & Switching</li>
                </ul>
              </div>
              <div className="px-5 py-4">
                <p className="mb-4">Every Data Center is a pool of resources. Networking holds pivotal role as it inter-connect all of data center resources together, thus it need to be scalable and efficient to connect hundreds of thousands of servers to handle the growing demands of cloud computing.</p>
              </div>

              <div className="px-8 pt-4 pb-8">
                <div className="grid grid-cols-2 gap-x-10 gap-y-6">
                  <div className="logo-partner"><Image src="/logos/polycom.png" width={280} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/aruba.png" width={268} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/zoom.png" width={323} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/ubiquiti.png" width={294} height={80} /></div>
                </div>
              </div>

            </div>

            <div id="cyber-security" className="flex flex-col">

              <div className="px-5 pt-12 pb-0 bg-primary text-white">
                <h1 className="uppercase text-2xl mb-6">Cyber Security<br /><span className="font-bold">Solutions</span></h1>
              </div>
              <div className="text-center"><Image src="/mobile/service-5-bg.png" width={600} height={348} /></div>
              <div className="-mt-2 px-5 pt-2 pb-5 bg-primary text-white">
                <ul className="list-infinite">
                  <li>Fire wall / Gateway Security</li>
                  <li>Endpoint Security</li>
                  <li>Security Information and Event Management (SIEM)</li>
                  <li>Virtualization Security</li>
                  <li>Sand boxing and Advanced Persistent Treat (APT)</li>
                  <li>Printer</li>
                  <li>Data Loss Prevention</li>
                </ul>
              </div>
              <div className="px-5 py-4">
                <p className="mb-4">Every company afraid to lost data and to improve business continuity we provide cyber security solution for protection network from illegitimate access or attach from hackers.</p>
              </div>

              <div className="px-8 pt-4 pb-8">
                <div className="grid grid-cols-3 gap-x-10 gap-y-6">
                  <div className="logo-partner"><Image src="/logos/symantec.png" width={299} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/sangfor.png" width={241} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/forcepoint.png" width={493} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/kaspersky.png" width={332} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/sophos.png" width={222} height={40} /></div>
                  <div className="logo-partner"><Image src="/logos/radware.png" width={276} height={50} /></div>
                  <div className="logo-partner"><Image src="/logos/mcafee.png" width={288} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/fortinet.png" width={512} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/f5.png" width={46} height={42} /></div>
                  <div className="logo-partner"><Image src="/logos/ibm.png" width={80} height={32} /></div>
                  <div className="logo-partner"><Image src="/logos/cisco.png" width={79} height={42} /></div>
                  <div className="logo-partner"><Image src="/logos/checkpoint.png" width={455} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/trendmicro.png" width={232} height={80} /></div>
                </div>
              </div>

            </div>

            <div id="other" className="flex flex-col">

              <div className="px-5 pt-12 pb-0 bg-primary text-white">
                <h1 className="uppercase text-2xl mb-6">Other<br /><span className="font-bold">Solutions</span></h1>
              </div>
              <div className="text-center"><Image src="/mobile/service-6-bg.png" width={600} height={600} /></div>
              <div className="px-5 py-4">
                <p className="mb-4">We also can provide other needs like monitoring center and outdoor panel and also interactive panel.</p>
                <ul className="list-infinite">
                  <li>Command Center</li>
                  <li>Video Surveillance</li>
                  <li>Interactive Panel</li>
                </ul>
              </div>

              <div className="px-8 pt-4 pb-8">
                <div className="grid grid-cols-3 gap-x-10 gap-y-6">
                  <div className="logo-partner"><Image src="/logos/datalite.png" width={291} height={50} /></div>
                  <div className="logo-partner"><Image src="/logos/lg.png" width={155} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/benq.png" width={89} height={48} /></div>
                </div>
              </div>

            </div>

            <div id="partners">
              <div className="flex flex-col border-t border-primary border-opacity-30 mt-6">
                <div className="w-full px-5 pt-16 pb-5">
                  <h2 className="text-2xl text-primary font-bold uppercase mb-6">Our Partners</h2>
                  <p className="text-xl text-gray-500 mb-6">From the people you trust, we work with the best partnerships from the best of the best.</p>
                </div>

                <div className="px-5 pb-10 grid grid-cols-3 gap-y-10 gap-x-10">
                  <div className="logo-partner"><Image src="/logos/polycom.png" width={280} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/canon.png" width={385} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/vertiv.png" width={313} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/juniper.png" width={280} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/dell.png" width={80} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/zoom.png" width={323} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/epson.png" width={339} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/apc.png" width={255} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/checkpoint.png" width={455} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/emc2.png" width={246} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/aruba.png" width={268} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/hp.png" width={80} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/vmware.png" width={378} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/rsa.png" width={228} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/commvault.png" width={460} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/ubiquiti.png" width={294} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/hpenterprise.png" width={188} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/kaspersky.png" width={332} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/ibm.png" width={200} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/veeam.png" width={444} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/mikrotik.png" width={306} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/fujitsu.png" width={161} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/sangfor.png" width={241} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/radware.png" width={276} height={50} /></div>
                  <div className="logo-partner"><Image src="/logos/veritas.png" width={406} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/acer.png" width={334} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/netapp.png" width={429} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/mcafee.png" width={288} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/f5.png" width={87} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/cisco.png" width={151} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/amp.png" width={116} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/lenovo.png" width={240} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/fortinet.png" width={512} height={80} /></div>
                  <div className="logo-partner"><Image src="/logos/datalite.png" width={291} height={50} /></div>
                </div>

                <div className="w-full px-5 pt-5 pb-5">
                  <p className="text-xl text-gray-500 mb-6">Whatever your company need, the possibilities of solutions we have on offer are endless.</p>
                </div>

              </div>
            </div>

            <div id="contact" className="bg-primary text-white">
              <div className="w-full px-5 pt-16 pb-16">
                <h2 className="text-2xl text-white font-bold uppercase mb-6">Contact Us</h2>
                <p className="mb-4"><span className="font-bold">PT. Alwindo Prima Solusi.</span><br />
                  The City Tower, 12 Floor Unit 1N <br />
                  JL. MH Thamrin No 81 Jakarta Pusat 10310, Indonesia </p>

                <p className="mb-4"><span className="font-bold">CALL US</span><br />
                +621 29490686 </p>

                <p className="mb-4"><span className="font-bold">EMAIL US</span><br />
                sales@.alwindo.co.id</p>
              </div>
            </div>

            <div className="px-4 py-4">
              <p className="text-center text-xs text-gray-500">Copyright © 2021 Alwindo Prima Solusi. <br />All Rights Reserved.  </p>
            </div>

          </div>
        </main>
      </>
    );
  } else {
    return (
      <>
        <Head>
          <title>Alwindo Prima Solusi</title>
          <meta name="description" content="Alwindo Prima Solusi. Commited to work & serve from the heart" />
        </Head>

        <main className="page-homepage hidden lg:block">
          <div className="hero-container">
            <div className="hero-text">
              <h1 className="uppercase">Commited to work & serve from the heart</h1>
            </div>
          </div>
        </main>

      </>
    );
  }

}

export default Home;
