import Head from 'next/head'

const ContactUs = () => {

  return (
    <>
      <Head>
        <title>Contact Us | Alwindo Prima Solusi</title>
        <meta name="description" content="PT. Alwindo Prima Solusi. The City Tower, 12 Floor Unit 1N. JL. MH Thamrin No 81 Jakarta Pusat 10310, Indonesia" />
      </Head>
      <main className="page-contact">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-text">
              <p>PT. Alwindo Prima Solusi.<br />
            The City Tower, 12 Floor Unit 1N <br />
            JL. MH Thamrin No 81 Jakarta Pusat 10310, Indonesia </p>

              <p>CALL US <br />
            +621 29490686 </p>

              <p>EMAIL US<br />
            sales@.alwindo.co.id</p>

            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default ContactUs;