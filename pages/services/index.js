import Head from 'next/head'
import Image from 'next/image'

const Services = () => {
  return (
    <>
      <Head>
        <title>Our Services | Alwindo Prima Solusi</title>
        <meta name="description" content="In creating the best infrastructure to your company information technology sector, the journey is not an instant growth. It has a parallel track to match ever evolving progress the company grows." />
      </Head>
      <main className="page-services">
        <div className="hero-container">

          <div className="flex px-8 py-12 gap-10 justify-between">
            <div className="max-w-xs">
              <div className="border-b border-white border-opacity-50 pb-5 mb-5">
                <h1 className="text-2xl uppercase">
                  <span className="font-semibold">THE JOURNEY</span><br />
                  <span className="font-light">IN CREATING YOUR IT INFRASTRUCTURE</span>
                </h1>
              </div>
              <p className="text-sm">In creating the best infrastructure to your company information technology sector, the journey is not an instant growth. It has a parallel track to match ever evolving progress the company grows.</p>
            </div>
            <div className="max-w-xs ml-24">
              <a href="/services/availability-data-protection" className="service-link mt-12">
                <div className="absolute -top-6 -left-3 bottom-0">
                  <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                  <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">3</div>
                </div>
                <div className="relative z-30 mb-3 -ml-5">
                  <Image src="/img-services-3c.png" width={360} height={180} />
                </div>
                <h2 className="text-xl uppercase mb-3">
                  <span className="font-light">Availability & Data Protection</span> <span className="font-semibold">Solutions</span>
                </h2>
                <p className="text-sm">Data protection is the process of safeguarding important information from corruption, compromise or loss. The importance of data protection increases as the amount of data created and stored continues to grow at unprecedented rates. There is also little tolerance for downtime that can make it impossible to access important information.</p>
              </a>
            </div>
            <div className="max-w-xs">
              <a href="/services/cyber-security" className="service-link mt-12">
                <div className="absolute -top-6 -left-3 bottom-0">
                  <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                  <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">5</div>
                </div>
                <div className="relative z-30 mb-3 -ml-16">
                  <Image src="/img-services-5b.png" width={360} height={189} />
                </div>
                <h2 className="text-xl uppercase mb-3">
                  <span className="font-light">Cyber Security</span> <span className="font-semibold">Solutions</span>
                </h2>
                <p className="text-sm">Cyber security refers to a company’s protection against unauthorized or criminal use of electronic data and cyber security services are the over-arching processes put in place to achieve this security and protect against common cyber threats.</p>
              </a>
            </div>
          </div>
          <div className="flex pl-32 pr-8 py-12 gap-10 xl:gap-20 justify-between">
            <div className="max-w-xs">
              <a href="/services/end-point" className="service-link -mt-40">
                <div className="absolute -top-6 -left-3 bottom-0">
                  <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                  <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">1</div>
                </div>
                <div className="relative z-30 mb-3 -ml-5 pt-6" style={{ maxWidth: 200 + 'px' }}>
                  <Image src="/img-services-1.png" width={300} height={265} />
                </div>
                <h2 className="text-xl uppercase mb-3">
                  <span className="font-light">End Point</span> <span className="font-semibold">Solutions</span>
                </h2>
                <p className="text-sm">Front end user computing refers to all of the customer need that handled along by user example: Personal computer, notebook/laptop, printing solutions and handheld devices.</p>
              </a>
            </div>

            <div className="max-w-xs">
              <a href="/services/data-center-infrastructure" className="service-link -mt-16">
                <div className="absolute -top-6 -left-3 bottom-0">
                  <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                  <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">2</div>
                </div>
                <div className="relative z-30 mb-3 -ml-5 pt-6" style={{ maxWidth: 240 + 'px', paddingLeft: 1 + 'px' }}>
                  <Image src="/img-services-2.png" width={300} height={242} />
                </div>
                <h2 className="text-xl uppercase mb-3">
                  <span className="font-light">Data Center & Infrastructure</span> <span className="font-semibold">Solutions</span>
                </h2>
                <p className="text-sm">Front end user computing refers to all of the customer need that handled along by user example: Personal computer, notebook/laptop, printing solutions and handheld devices.</p>
              </a>
            </div>

            <div className="max-w-xs">
              <a href="/services/network" className="service-link mt-4">
                <div className="absolute -top-6 -left-3 bottom-0">
                  <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                  <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">4</div>
                </div>
                <div className="relative z-30 mb-3 -ml-5 pt-0" style={{ maxWidth: 360 + 'px', paddingLeft: 10 + 'px' }}>
                  <Image src="/img-services-4.png" width={360} height={264} />
                </div>
                <h2 className="text-xl uppercase mb-3">
                  <span className="font-light">Network</span> <span className="font-semibold">Solutions</span>
                </h2>
                <p className="text-sm">Solutions that has critical role in business and communications. Networks provide speed, connectivity and ultimately value to their members and solutions to business challenges.</p>
              </a>
            </div>

            <div className="max-w-xs">
              <a href="/services/other" className="service-link mt-16">
                <div className="absolute -top-6 -left-3 bottom-0">
                  <div className="border-l border-white border-opacity-50 h-full absolute top-0 left-3 z-10"></div>
                  <div className="relative inline-block w-6 h-6 leading-6 text-center border border-white border-opacity-50 rounded-full bg-primary z-20">6</div>
                </div>
                <div className="relative z-30 mb-3 -ml-10 pt-0" style={{ maxWidth: 220 + 'px' }}>
                  <Image src="/img-services-6.png" width={300} height={276} />
                </div>
                <h2 className="text-xl uppercase mb-3">
                  <span className="font-light">OTHER</span> <span className="font-semibold">Solutions</span>
                </h2>
                <p className="text-sm">Our other solutions.</p>
              </a>
            </div>

          </div>
        </div>
      </main>
    </>
  );
}

export default Services;