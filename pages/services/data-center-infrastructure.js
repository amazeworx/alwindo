import Head from 'next/head'
import Image from 'next/image'

const DataCenterInfrastructure = () => {
  return (
    <>
      <Head>
        <title>Data Center Infrastructure Solutions | Alwindo Prima Solusi</title>
        <meta name="description" content="Data Center contain many IT peripherals, computer engine is one of it, popular being called as servers. As time goes by, technologies has grown wider, trying to integrate one to another." />
      </Head>
      <main className="page-data-center-infrastructure">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-title"><h1 className="uppercase">Data Center Infrastructure<br /><span className="font-bold">Solutions</span></h1></div>
            <div className="hero-text">
              <p>Data Center contain many IT peripherals, computer engine is one of it, popular being called as servers. As time goes by, technologies has grown wider, trying to integrate one to another.</p>
              <ul>
                <li>Servers</li>
                <li>Integrated Infrastructure System</li>
                <li>Hyper converged System</li>
              </ul>
              <p className="mt-12">The z Storage is the computerized data storage system for large scale, intended as centralized storage (consolidation) system in every organization to store billions of data.</p>
              <ul>
                <li>Storage Area Network (Block Based Storage)</li>
                <li>Network Attached Storage (File Based Storage)</li>
                <li>Object Storage (Object Based Storage)</li>
                <li>Backup Storage (Storage for Backup Solution)</li>
              </ul>
            </div>
            <div className="hero-logos">
              <div className="grid grid-cols-4 gap-10">
                <div className="logo-partner"></div>
                <div className="logo-partner"><Image src="/logos/vertiv.png" width={313} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/fujitsu.png" width={161} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/netapp.png" width={429} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/apc.png" width={255} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/lenovo.png" width={240} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/dellemc.png" width={463} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/vmware.png" width={378} height={80} /></div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default DataCenterInfrastructure;