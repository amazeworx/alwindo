import Head from 'next/head'
import Image from 'next/image'

const Other = () => {
  return (
    <>
      <Head>
        <title>Other Solutions | Alwindo Prima Solusi</title>
        <meta name="description" content="We also can provide other needs like monitoring center and outdoor panel and also interactive panel." />
      </Head>
      <main className="page-other">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-title"><h1 className="uppercase">Other<br /><span className="font-bold">Solutions</span></h1></div>
            <div className="hero-text">
              <p>We also can provide other needs like monitoring center and outdoor panel and also interactive panel.</p>
              <ul>
                <li>Command Center</li>
                <li>Video Surveillance</li>
                <li>Interactive Panel</li>
              </ul>
            </div>
            <div className="hero-logos">
              <div className="grid grid-cols-3 gap-20">
                <div className="logo-partner"><Image src="/logos/datalite.png" width={291} height={50} /></div>
                <div className="logo-partner"><Image src="/logos/lg.png" width={155} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/benq.png" width={89} height={48} /></div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default Other;