import Head from 'next/head'
import Image from 'next/image'

const CyberSecurity = () => {
  return (
    <>
      <Head>
        <title>Cyber Security Solutions | Alwindo Prima Solusi</title>
        <meta name="description" content="Every company afraid to lost data and to improve business continuity we provide cyber security solution for protection network from illegitimate access or attach from hackers." />
      </Head>
      <main className="page-cyber-security">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-title"><h1 className="uppercase">Cyber Security<br /><span className="font-bold">Solutions</span></h1></div>
            <div className="hero-text">
              <p>Every company afraid to lost data and to improve business continuity we provide cyber security solution for protection network from illegitimate access or attach from hackers.</p>
              <ul>
                <li>Fire wall / Gateway Security</li>
                <li>Endpoint Security</li>
                <li>Security Information and Event Management (SIEM)</li>
                <li>Virtualization Security</li>
                <li>Sand boxing and Advanced Persistent Treat (APT)</li>
                <li>Printer</li>
                <li>Data Loss Prevention</li>
              </ul>
            </div>
            <div className="hero-logos">
              <div className="grid grid-cols-5 gap-10">
                <div className="logo-partner"></div>
                <div className="logo-partner"></div>
                <div className="logo-partner"></div>
                <div className="logo-partner"><Image src="/logos/symantec.png" width={299} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/sangfor.png" width={241} height={80} /></div>
                <div className="logo-partner"></div>
                <div className="logo-partner"></div>
                <div className="logo-partner"></div>
                <div className="logo-partner"><Image src="/logos/forcepoint.png" width={493} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/kaspersky.png" width={332} height={80} /></div>
                <div className="logo-partner"></div>
                <div className="logo-partner"><Image src="/logos/sophos.png" width={222} height={40} /></div>
                <div className="logo-partner"><Image src="/logos/radware.png" width={276} height={50} /></div>
                <div className="logo-partner"><Image src="/logos/mcafee.png" width={288} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/fortinet.png" width={512} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/f5.png" width={46} height={42} /></div>
                <div className="logo-partner"><Image src="/logos/ibm.png" width={80} height={32} /></div>
                <div className="logo-partner"><Image src="/logos/cisco.png" width={79} height={42} /></div>
                <div className="logo-partner"><Image src="/logos/checkpoint.png" width={455} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/trendmicro.png" width={232} height={80} /></div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default CyberSecurity;