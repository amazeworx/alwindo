import Head from 'next/head'
import Image from 'next/image'

const EndPoint = () => {
  return (
    <>
      <Head>
        <title>End Point Solutions | Alwindo Prima Solusi</title>
        <meta name="description" content="In creating the best infrastructure to your company information technology sector, the journey is not an instant growth. It has a parallel track to match ever evolving progress the company grows." />
      </Head>
      <main className="page-end-point">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-title"><h1 className="uppercase">End Point<br /><span className="font-bold">Solutions</span></h1></div>
            <div className="hero-text">
              <p>The equipment that being used directly by end-user to support their daily work activities.</p>
              <ul>
                <li>Desktops</li>
                <li>Workstation</li>
                <li>Laptops</li>
                <li>Thin Client</li>
                <li>Virtual Desktop</li>
                <li>Printer</li>
                <li>Fingerprint Scanner</li>
              </ul>
            </div>
            <div className="hero-logos">
              <div className="grid grid-cols-3 gap-10">
                <div className="logo-partner"></div>
                <div className="logo-partner"><Image src="/logos/acer.png" width={334} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/lenovo.png" width={240} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/dell.png" width={80} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/fujitsu.png" width={161} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/canon.png" width={385} height={80} /></div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default EndPoint;