import Head from 'next/head'
import Image from 'next/image'

const Network = () => {
  return (
    <>
      <Head>
        <title>Network Solutions | Alwindo Prima Solusi</title>
        <meta name="description" content="Every Data Center is a pool of resources. Networking holds pivotal role as it inter-connect all of data center resources together, thus it need to be scalable and efficient to connect hundreds of thousands of servers to handle the growing demands of cloud computing." />
      </Head>
      <main className="page-network">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-title"><h1 className="uppercase">Network<br /><span className="font-bold">Solutions</span></h1></div>
            <div className="hero-text">
              <p>Every Data Center is a pool of resources. Networking holds pivotal role as it inter-connect all of data center resources together, thus it need to be scalable and efficient to connect hundreds of thousands of servers to handle the growing demands of cloud computing.</p>
              <ul>
                <li>WAN Optimization</li>
                <li>Wireless Network</li>
                <li>Network Operation Control</li>
                <li>Application Delivery Control/ Load Balance</li>
                <li>Routing & Switching</li>
              </ul>
              <div className="mt-6 flex justify-center">
                <Image src="/services-4-img-2b.png" width={300} height={280} />
              </div>
            </div>
            <div className="hero-logos">
              <div className="grid grid-cols-4 gap-12">
                <div className="logo-partner"><Image src="/logos/polycom.png" width={280} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/aruba.png" width={268} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/zoom.png" width={323} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/ubiquiti.png" width={294} height={80} /></div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default Network;