import Head from 'next/head'
import Image from 'next/image'

const AvailabilityDataProtection = () => {
  return (
    <>
      <Head>
        <title>Backup Continuity Solutions | Alwindo Prima Solusi</title>
        <meta name="description" content="Every organization expect their businesses will always up and running. But there are few unpredictable things such as natural disaster, terrorism or unplanned system downtime." />
      </Head>
      <main className="page-availability-data-protection">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-title"><h1 className="uppercase">Backup Continuity <br /><span className="font-bold">Solutions</span></h1></div>
            <div className="hero-text">
              <p>Every organization expect their businesses will always up and running. But there are few unpredictable things such as natural disaster, terrorism or unplanned system downtime. There are many technologies that can be use to prevent the disruption and to allow fast recovery in business critical application.</p>
              <ul>
                <li>Replication</li>
                <li>High Availability Cluster</li>
                <li>Fault Tolerance</li>
                <li>Active-Active Data Center</li>
                <li>Backup and recovery</li>
                <li>Archiving</li>
              </ul>
            </div>
            <div className="hero-logos">
              <div className="grid grid-cols-2 gap-10">
                <div className="logo-partner"><Image src="/logos/commvault.png" width={460} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/dellemc.png" width={463} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/veeam.png" width={444} height={80} /></div>
                <div className="logo-partner"><Image src="/logos/veritas.png" width={406} height={80} /></div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default AvailabilityDataProtection;