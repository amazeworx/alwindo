import Head from 'next/head'
import Image from 'next/image'

const OurPartners = () => {
  return (
    <>
      <Head>
        <title>Our Partners | Alwindo Prima Solusi</title>
        <meta name="description" content="Alwindo Prima Solusi Partners." />
      </Head>
      <main className="page-partners">
        <div className="pt-12 pb-12 px-4 max-w-7xl mx-auto">
          <div className="grid grid-cols-5 gap-y-24 gap-x-32">
            <div className="logo-partner"><Image src="/logos/polycom.png" width={280} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/canon.png" width={385} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/vertiv.png" width={313} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/juniper.png" width={280} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/dell.png" width={80} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/zoom.png" width={323} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/epson.png" width={339} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/apc.png" width={255} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/checkpoint.png" width={455} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/emc2.png" width={246} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/aruba.png" width={268} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/hp.png" width={80} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/vmware.png" width={378} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/rsa.png" width={228} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/commvault.png" width={460} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/ubiquiti.png" width={294} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/hpenterprise.png" width={188} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/kaspersky.png" width={332} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/ibm.png" width={200} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/veeam.png" width={444} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/mikrotik.png" width={306} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/fujitsu.png" width={161} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/sangfor.png" width={241} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/radware.png" width={276} height={50} /></div>
            <div className="logo-partner"><Image src="/logos/veritas.png" width={406} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/acer.png" width={334} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/netapp.png" width={429} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/mcafee.png" width={288} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/f5.png" width={87} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/cisco.png" width={151} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/amp.png" width={116} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/lenovo.png" width={240} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/fortinet.png" width={512} height={80} /></div>
            <div className="logo-partner"><Image src="/logos/datalite.png" width={291} height={50} /></div>
          </div>
        </div>
      </main>
    </>
  );
}

export default OurPartners;