import Head from 'next/head'

const VisionMission = () => {
  return (
    <>
      <Head>
        <title>Vision & Mission | Alwindo Prima Solusi</title>
        <meta name="description" content="Vision & Mission Alwindo Prima Solusi. Our vision is to lead the world with the technology of the future." />
      </Head>
      <main className="page-vision-mission">
        <div className="hero-container">
          <div className="hero-inner">
            <div className="hero-text">
              <h2 className="text-primary uppercase font-semibold">Vision</h2>
              <p>Our vision is to lead the world with the technology of the future.</p>
              <h2 className="text-primary uppercase font-semibold mt-6">Mission</h2>
              <p>We strive to be the leading IT solution company with commitment to support our customer by providing innovative, reliable and best-in-class IT solutions to help their business go further.</p>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}

export default VisionMission;