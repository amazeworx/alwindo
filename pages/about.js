import Head from 'next/head'

const About = () => {

  return (
    <>
      <Head>
        <title>About Us | Alwindo Prima Solusi</title>
        <meta name="description" content="About Alwindo Prima Solusi. Founded in 2021, ALWINDO PRIMA SOLUSI  is one of the fastest growing IT Solution Provider. APS offers a comprehensive range of capabilities." />
      </Head>
      <main className="page-about">
        <div className="hero-container">
          <div className="hero-text">
            <p>Founded in 2021, ALWINDO PRIMA SOLUSI  is one of the fastest growing IT Solution Provider. APS offers a comprehensive range of capabilities. APS provides and actively pursue innovative technologies and new business opportunities that enable businesses to make an effective transition to true business environment. APS enjoys an established track record of excellent support & service standards with our customers.</p>
          </div>
        </div>
      </main>
    </>
  );
}

export default About;