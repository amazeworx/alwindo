import Head from 'next/head'
const { default: Navbar } = require("./Navbar")

const Layout = ({ children }) => {
  return (
    <div className="page">
      <Head>
        <link rel="icon" href="/logo-alwindo.svg" />
      </Head>
      <Navbar />
      { children}
    </div>
  );
}

export default Layout;