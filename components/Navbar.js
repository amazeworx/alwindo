import Link from 'next/link'
import Image from 'next/image'
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const Navbar = () => {
  const router = useRouter();

  const [isOpen, setisOpen] = useState(false);

  function handleClick() {
    setisOpen(!isOpen);
  }

  const handleScroll = () => {
    if (window.scrollY > 20) {
      document.querySelector(".mobilenavbar").className = "mobilenavbar scroll";
    } else {
      document.querySelector(".mobilenavbar").className = "mobilenavbar";
    }
  };

  useEffect(() => {
    function watchScroll() {
      window.addEventListener("scroll", handleScroll);
    }
    watchScroll();
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  let serviceMenuSetActive = '';
  if (
    (router.pathname == '/services') ||
    (router.pathname == '/services/end-point') ||
    (router.pathname == '/services/data-center-infrastructure') ||
    (router.pathname == '/services/availability-data-protection') ||
    (router.pathname == '/services/cyber-security') ||
    (router.pathname == '/services/network') ||
    (router.pathname == '/services/other')
  ) {
    serviceMenuSetActive = "active"
  }

  return (
    <>
      <nav className="h-36 hidden lg:block">
        <div className="max-w-7xl pt-4 py-2 px-4 mx-auto">
          <div className="w-full">
            <Link href="/">
              <a className="inline-block pb-2 ml-2 w-14"><Image src="/logo-alwindo.svg" width={100} height={124} /></a>
            </Link>
          </div>
          <div className="w-full">
            <div className="navbar">
              <Link href="/about"><a className={`navbar-link ${router.pathname == '/about' ? 'active' : ''}`}>About Us</a></Link>
              <Link href="/vision-mission"><a className={`navbar-link ${router.pathname == '/vision-mission' ? 'active' : ''}`}>Vision &amp; Mission</a></Link>
              <ul className="navbar-dropdown">
                <li>
                  <Link href="/services"><a className={`navbar-link ${serviceMenuSetActive}`}>Services</a></Link>
                  <ul className="navbar-submenu">
                    <li><Link href="/services/end-point"><a className={`navbar-submenu-link ${router.pathname == '/services/end-point' ? 'active' : ''}`}>End Point Solutions</a></Link></li>
                    <li><Link href="/services/data-center-infrastructure"><a className={`navbar-submenu-link ${router.pathname == '/services/data-center-infrastructure' ? 'active' : ''}`}>Data Center &amp; Infrastructure</a></Link></li>
                    <li><Link href="/services/availability-data-protection"><a className={`navbar-submenu-link ${router.pathname == '/services/availability-data-protection' ? 'active' : ''}`}>Availability &amp; Data Protection</a></Link></li>
                    <li><Link href="/services/network"><a className={`navbar-submenu-link ${router.pathname == '/services/network' ? 'active' : ''}`}>Network Solutions</a></Link></li>
                    <li><Link href="/services/cyber-security"><a className={`navbar-submenu-link ${router.pathname == '/services/cyber-security' ? 'active' : ''}`}>Cyber Security Solutions</a></Link></li>
                    <li><Link href="/services/other"><a className={`navbar-submenu-link ${router.pathname == '/services/other' ? 'active' : ''}`}>Other Solutions</a></Link></li>
                  </ul>
                </li>
              </ul>
              <Link href="/our-partners"><a className={`navbar-link ${router.pathname == '/our-partners' ? 'active' : ''}`}>Our Partners</a></Link>
              <Link href="/contact-us"><a className={`navbar-link ${router.pathname == '/contact-us' ? 'active' : ''}`}>Contact Us</a></Link>
            </div>
          </div>
        </div>
      </nav >

      <div className="lg:hidden fixed top-0 left-0 right-0 z-30">
        <div className="mobilenavbar">
          <div className="flex px-4 py-4 w-full items-center">
            <button type="button" className="mr-4" id="open-menu" onClick={handleClick}>
              <svg className="menu-icon" width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M3 4H21V6H3V4ZM3 11H21V13H3V11ZM3 18H21V20H3V18Z" /></svg>
            </button>
            <a href="/" className="flex items-end">
              <div className="inline-block">
                <svg className="navbar-logo" width="500" height="620" viewBox="0 0 500 620" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M247.822 0L500 620H343.121L247.822 385.634L154.175 620H0L247.822 0Z" />
                </svg>
              </div>
              <div className="navbar-title inline-block ml-1">ALWINDO PRIMA SOLUSI</div>
            </a>

          </div>
        </div>
      </div>

      <nav id="mobilemenu" className={`mobilemenu ${isOpen ? "open" : ""}`}>
        <div className="p-4">
          <div className="text-right">
            <button type="button" id="close-menu" onClick={handleClick}>
              <svg className="fill-current text-white" width="32" height="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 10.586L16.95 5.636L18.364 7.05L13.414 12L18.364 16.95L16.95 18.364L12 13.414L7.05 18.364L5.636 16.95L10.586 12L5.636 7.05L7.05 5.636L12 10.586Z" />
              </svg>
            </button>

          </div>
          <ul className="text-lg">
            <li><a className="inline-block uppercase py-2" href="#about" onClick={handleClick}>About Us</a></li>
            <li><a className="inline-block uppercase py-2" href="#vision-mission" onClick={handleClick}>Vision &amp; Mission</a></li>
            <li><a className="inline-block uppercase py-2" href="#services" onClick={handleClick}>Our Services</a></li>
            <li><a className="inline-block uppercase py-2" href="#partners" onClick={handleClick}>Our Partners</a></li>
            <li><a className="inline-block uppercase py-2" href="#contact" onClick={handleClick}>Contact Us</a></li>
          </ul>
        </div>
      </nav>
      <div id="overlay" className={`overlay ${isOpen ? "open" : ""}`}></div>

    </>
  );
}

export default Navbar;