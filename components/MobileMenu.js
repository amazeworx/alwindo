const MobileMenu = ({ open }) => {
  return (
    <>
      <nav id="mobilemenu" className="mobilemenu">
        <div className="p-4">
          <div className="text-right">
            <button type="button" id="close-menu">
              <svg className="fill-current text-white" width="32" height="32" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 10.586L16.95 5.636L18.364 7.05L13.414 12L18.364 16.95L16.95 18.364L12 13.414L7.05 18.364L5.636 16.95L10.586 12L5.636 7.05L7.05 5.636L12 10.586Z" />
              </svg>
            </button>

          </div>
          <ul className="text-lg">
            <li><a className="inline-block uppercase py-2" href="#about">About Us</a></li>
            <li><a className="inline-block uppercase py-2" href="#vision-mission">Vision &amp; Mission</a></li>
            <li><a className="inline-block uppercase py-2" href="#services">Our Services</a></li>
            <li><a className="inline-block uppercase py-2" href="#partners">Our Partners</a></li>
            <li><a className="inline-block uppercase py-2" href="#contact">Contact Us</a></li>
          </ul>
        </div>
      </nav>
      <div id="overlay" className="overlay"></div>
    </>
  );
}

export default MobileMenu;